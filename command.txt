// kubectl login gitlab registry
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=/home/ngoctrong102/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson

// install gitlab runner
helm install --namespace gitlab-runner gitlab-runner -f gitlab_runner/values.yaml gitlab/gitlab-runner

// thêm quyền cho gitlab runner
kubectl create rolebinding default-view --clusterrole=edit --serviceaccount=gitlab-runner:default --namespace=gitlab-runner

//docker executor login gitlab registry
kubectl create configmap docker-client-config --namespace gitlab-runner --from-file ~/.docker/config.json



    - kubectl expose deployment/hello-express --port=8769 --target-port=3000 --type=LoadBalancer --external-ip=34.68.107.80