FROM node:16.14.2-alpine
COPY . .
RUN sudo sysctl fs.inotify.max_user_watches=582222 && sudo sysctl -p
RUN npm install
ENTRYPOINT ["npm","start"]
